unit nsis;

interface

uses
  windows, gdipCheckBox, gdipButton, main, RgnUnit, for_png, addfunc;

type
  VarConstants = (
    INST_0,       // $0
    INST_1,       // $1
    INST_2,       // $2
    INST_3,       // $3
    INST_4,       // $4
    INST_5,       // $5
    INST_6,       // $6
    INST_7,       // $7
    INST_8,       // $8
    INST_9,       // $9
    INST_R0,      // $R0
    INST_R1,      // $R1
    INST_R2,      // $R2
    INST_R3,      // $R3
    INST_R4,      // $R4
    INST_R5,      // $R5
    INST_R6,      // $R6
    INST_R7,      // $R7
    INST_R8,      // $R8
    INST_R9,      // $R9
    INST_CMDLINE, // $CMDLINE
    INST_INSTDIR, // $INSTDIR
    INST_OUTDIR,  // $OUTDIR
    INST_EXEDIR,  // $EXEDIR
    INST_LANG,    // $LANGUAGE
    __INST_LAST
    );
  TVariableList = INST_0..__INST_LAST;
  TExecuteCodeSegment = function (const funct_id: Integer; const parent: HWND): Integer;  stdcall;
  
  pextrap_t = ^extrap_t;
  extrap_t = record
    exec_flags: Pointer; // exec_flags_t;
    exec_code_segment: Pointer; //  TFarProc;
    validate_filename: Pointer; // Tvalidate_filename;
    RegisterPluginCallback: Pointer; //TRegisterPluginCallback;
  end;
  
  pstack_t = ^stack_t;
  stack_t = record
    next: pstack_t;
    text: PChar;
  end;

var
  g_stringsize: integer;
  g_stacktop: ^pstack_t;
  g_variables: PChar;
  g_hwndParent: HWND;
  g_extraparameters: pextrap_t;
  func : TExecuteCodeSegment;
  extrap : extrap_t;

//procedure Init(const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer);
procedure Init( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil );
function PopString(): string;
function PopInt(): integer;
procedure PushString(const str: string='');
procedure Pushint( const num: integer );
function Call(NSIS_func : integer) : Integer;

//------------------------------------------------------------------------------
function IntToStr_( num: integer ): string;
function StrToInt_(s:string):integer;
//------------------------------------------------------------------------------
procedure CheckBoxCreate_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
procedure CheckBoxRefresh_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
procedure CheckBoxSetText_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
procedure CheckBoxGetVisibility_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
procedure CheckBoxSetVisibility_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
procedure CheckBoxGetEnabled_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
procedure CheckBoxSetEnabled_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
procedure CheckBoxSetFont_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
procedure CheckBoxSetFontColor_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
procedure CheckBoxSetChecked_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
procedure CheckBoxGetChecked_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
procedure CheckBoxSetEvent_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
procedure CheckBoxSetPosition_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
procedure CheckBoxGetPosition_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
procedure CheckBoxSetCursor_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
//------------------------------------------------------------------------------
procedure BtnCreate_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
procedure ReplaceOldBtn_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
procedure BtnRefresh_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
procedure BtnSetText_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
procedure BtnGetText_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
procedure BtnSetTextAlignment_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
procedure BtnGetVisibility_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
procedure BtnSetVisibility_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
procedure BtnGetEnabled_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
procedure BtnSetEnabled_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
procedure BtnSetFont_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
procedure BtnSetFontColor_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
procedure BtnGetChecked_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
procedure BtnSetChecked_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
procedure BtnSetEvent_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
procedure BtnSetPosition_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
procedure BtnGetPosition_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
procedure BtnSetCursor_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
procedure GetSysCursorHandle_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
//------------------------------------------------------------------------------
procedure ImgLoad_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
procedure ImgSetPosition_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
procedure ImgGetPosition_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
procedure ImgSetVisibility_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
procedure ImgGetVisibility_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
procedure ImgSetVisiblePart_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
procedure ImgGetVisiblePart_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
procedure ImgSetTransparent_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
procedure ImgGetTransparent_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
procedure ImgRelease_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
procedure ImgApplyChanges_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
//------------------------------------------------------------------------------
procedure CreateFormFromImage_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
procedure CreateBitmapRgn_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
procedure SetMinimizeAnimation_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
procedure GetMinimizeAnimation_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
//------------------------------------------------------------------------------
procedure gdipShutdown_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;

procedure TestUnicode_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;

implementation

//------------------------------------------------------------------------------
{function IntToStr( num: integer ): string;
var
  mStr: string;
begin
  wsprintf( PChar( mStr ), '%d', Pointer( num ) );
  Result := mStr;
end;}

function IntToStr_( num: integer ): string;
var
  s: string;
begin
  Str( num, s );
  Result := s;
end;

function StrToInt_(s:string):integer;
var
  t:integer;
  c:integer;
begin
  val(s,t,c);
  if c=0 then
    Result:=t
  else
    Result:=0;
end;
//------------------------------------------------------------------------------

procedure Init( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil );
begin
  g_stringsize 		:= string_size;
  g_hwndParent 		:= hwndParent;
  g_stacktop   		:= stacktop;
  g_variables  		:= variables;
  g_extraparameters	:= extraparameters;
  //extrap := g_extraparameters^;
end;

function Call(NSIS_func : integer) : Integer;
begin
	Result := 0;
	if ( NSIS_func <> 0 ) and ( g_extraparameters <> nil ) then
	begin
		@func := g_extraparameters^.exec_code_segment;
		NSIS_func := NSIS_func - 1;
		Result := func( NSIS_func, g_hwndParent );
	end;
 { NSISFun := StrToIntDef(NSIS_func, 0);
  if (NSISFun <> 0) and (g_extraparameters <> nil) then
    begin
    @func := extrap.exec_code_segment;
    NSISFun := NSISFun - 1;
    Result := func(NSISFun, g_hwndParent);
    end;}
end;

function PopString(): string;
var
  th: pstack_t;
begin
  if integer(g_stacktop^) <> 0 then
  begin
    th := g_stacktop^;
    Result := PChar(@th.text);
    g_stacktop^ := th.next;
    GlobalFree(HGLOBAL(th));
  end;
end;

function PopInt(): integer;
var
  th: pstack_t;
begin
  Result := 0;
  if integer(g_stacktop^) <> 0 then
  begin
    th := g_stacktop^;
    Result := StrToInt_( PChar(@th.text) );
    g_stacktop^ := th.next;
    GlobalFree(HGLOBAL(th));
  end;
end;

{-----}

procedure PushString(const str: string='');
var
  th: pstack_t;
begin
  if integer(g_stacktop) <> 0 then
  begin
    th := pstack_t(GlobalAlloc(GPTR, SizeOf(stack_t) + g_stringsize));
    lstrcpyn(@th.text, PChar(str), g_stringsize);
    th.next := g_stacktop^;
    g_stacktop^ := th;
  end;
end;

procedure Pushint( const num: integer );
var
  th: pstack_t;
  str: string;
begin
  if integer(g_stacktop) <> 0 then
  begin
    str := IntToStr_( num );
    th := pstack_t(GlobalAlloc(GPTR, SizeOf(stack_t) + g_stringsize));
    lstrcpyn(@th.text, PChar(str), g_stringsize);
    th.next := g_stacktop^;
    g_stacktop^ := th;
  end;
end;

{-----}

function GetUserVariable(const varnum: TVariableList): string;
begin
  if (integer(varnum) >= 0) and (integer(varnum) < integer(__INST_LAST)) then
  begin
    Result := g_variables + integer(varnum) * g_stringsize;
  end
  else
  begin
    Result := '';
  end;  
end;

{-----}

procedure SetUserVariable(const varnum: TVariableList; const value: string);
begin
  if (value <> '') and (integer(varnum) >= 0) and (integer(varnum) < integer(__INST_LAST)) then
  begin
    lstrcpy(g_variables + integer(varnum) * g_stringsize, PChar(value));
  end;  
end;

//------------------------------------------------------------------------------
procedure CheckBoxCreate_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
var
  Wnd, h: HWND;
  Left,Top,Width,Height: integer;
  FileName: string;
  GroupID, TextIndent: integer;
begin
  Init( hwndParent, string_size, variables, stacktop, extraparameters );
  h := PopInt;
  FileName := PopString;
  Left := PopInt;
  Top := PopInt;
  Width := PopInt;
  Height := PopInt;
  GroupID := PopInt;
  TextIndent := PopInt;

  Wnd := CheckBoxCreate( h, Left, Top, Width, Height, PChar( FileName ), GroupID, TextIndent );
  PushInt( Wnd );
end;

procedure CheckBoxRefresh_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
var
  h: HWND;
begin
  Init( hwndParent, string_size, variables, stacktop, extraparameters );
  h := PopInt;

  CheckBoxRefresh( h );
end;

procedure CheckBoxSetText_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
var
  h: HWND;
  Text: string;
begin
  Init( hwndParent, string_size, variables, stacktop, extraparameters );
  h := PopInt;
  Text := PopString;

  CheckBoxSetText( h, PChar( Text ) );
end;

procedure CheckBoxGetText_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
var
  h: HWND;
  Text: PChar;
  Size: integer;
begin
  Init( hwndParent, string_size, variables, stacktop, extraparameters );
  h := PopInt;
   Text := '';
  CheckBoxGetText( h, Text, Size );
  PushString( Text );
end;

procedure CheckBoxGetVisibility_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
var
  h: HWND;
  return: boolean;
begin
  Init( hwndParent, string_size, variables, stacktop, extraparameters );
  h := PopInt;

  return := CheckBoxGetVisibility( h );
  PushInt( integer( return ) );
end;

procedure CheckBoxSetVisibility_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
var
  h: HWND;
  Value: boolean;
begin
  Init( hwndParent, string_size, variables, stacktop, extraparameters );
  h := PopInt;
  Value := boolean( PopInt );

  CheckBoxSetVisibility( h, Value );
end;

procedure CheckBoxGetEnabled_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
var
  h: HWND;
  return: boolean;
begin
  Init( hwndParent, string_size, variables, stacktop, extraparameters );
  h := PopInt;

  return := CheckBoxGetEnabled( h );
  PushInt( integer( return ) );
end;

procedure CheckBoxSetEnabled_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
var
  h: HWND;
  Value: boolean;
begin
  Init( hwndParent, string_size, variables, stacktop, extraparameters );
  h := PopInt;
  Value := boolean( PopInt );

  BtnSetEnabled( h, Value );
end;

procedure CheckBoxSetFont_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
var
  h: HWND;
  Font: HFONT;
begin
  Init( hwndParent, string_size, variables, stacktop, extraparameters );
  h := PopInt;
  Font := HFONT( PopInt );

  CheckBoxSetFont( h, Font );
end;

procedure CheckBoxSetFontColor_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
var
  h: HWND;
  NormalFontColor, FocusedFontColor, PressedFontColor, DisabledFontColor: integer;
begin
  Init( hwndParent, string_size, variables, stacktop, extraparameters );
  h := PopInt;
  NormalFontColor := PopInt;
  FocusedFontColor := PopInt;
  PressedFontColor := PopInt;
  DisabledFontColor := PopInt;

  CheckBoxSetFontColor( h, NormalFontColor, FocusedFontColor, PressedFontColor, DisabledFontColor );
end;

procedure CheckBoxSetChecked_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
var
  h: HWND;
  Value: boolean;
begin
  Init( hwndParent, string_size, variables, stacktop, extraparameters );
  h := PopInt;
  Value := boolean( PopInt );

  CheckBoxSetChecked( h, Value );
end;

procedure CheckBoxGetChecked_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
var
  h: HWND;
  return: boolean;
begin
  Init( hwndParent, string_size, variables, stacktop, extraparameters );
  h := PopInt;

  return := CheckBoxGetChecked( h );
  PushInt( integer( return ) );
end;

procedure CheckBoxSetEvent_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
var
  h: HWND;
  EventID, Event: integer;
begin
  Init( hwndParent, string_size, variables, stacktop, extraparameters );
  h := PopInt;
  EventID := PopInt;
  Event := PopInt;

  CheckBoxSetEvent( h, EventID, Event );
end;

procedure CheckBoxSetPosition_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
var
  h: HWND;
  NewLeft, NewTop, NewWidth, NewHeight: integer;
begin
  Init( hwndParent, string_size, variables, stacktop, extraparameters );
  h := PopInt;
  NewLeft := PopInt;
  NewTop := PopInt;
  NewWidth := PopInt;
  NewHeight := PopInt;

  CheckBoxSetPosition( h, NewLeft, NewTop, NewWidth, NewHeight );
end;

procedure CheckBoxGetPosition_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
var
  h: HWND;
  Left, Top, Width, Height: integer;
begin
  Init( hwndParent, string_size, variables, stacktop, extraparameters );
  h := PopInt;

  CheckBoxGetPosition( h, Left, Top, Width, Height );
  PushInt( Left );
  PushInt( Top );
  PushInt( Width );
  PushInt( Height );
end;

procedure CheckBoxSetCursor_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
var
  h: HWND;
  hCur: HICON;
begin
  Init( hwndParent, string_size, variables, stacktop, extraparameters );
  h := PopInt;
  hCur := HICON( PopInt );

  CheckBoxSetCursor( h, hCur );
end;
//------------------------------------------------------------------------------

procedure BtnCreate_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
var
  hParent: HWND;
  Left,Top,Width,Height: integer;
  FileName: string;
  ShadowWidth: integer;
  IsCheckBtn: boolean;
  return: HWND;
begin
  Init( hwndParent, string_size, variables, stacktop, extraparameters );
  hParent := PopInt;
  FileName := PopString;
  Left := PopInt;
  Top := PopInt;
  Width := PopInt;
  Height := PopInt;
  ShadowWidth := PopInt;
  IsCheckBtn := boolean( PopInt );

  return := BtnCreate( hParent, Left, Top, Width, Height, PChar( FileName ), ShadowWidth, IsCheckBtn );
  PushInt( return );
end;

procedure ReplaceOldBtn_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
var
  Left,Top,Width,Height: integer;
  FileName: string;
  return: HWND;
  
  BtnNum, BtnLen: integer;
  BtnWnd: HWND;
  BtnRect: TRect;
  BtnOldRect: TBtnRect;
  BtnText: string;
begin
  Init( hwndParent, string_size, variables, stacktop, extraparameters );
  {hParent := PopInt;
  FileName := PopString;
  Left := PopInt;
  Top := PopInt;
  Width := PopInt;
  Height := PopInt;
  ShadowWidth := PopInt;
  IsCheckBtn := boolean( PopInt );}

  BtnNum := PopInt;
  FileName := PopString;
  Left := PopInt;
  Top := PopInt;
  Width := PopInt;
  Height := PopInt;
  
  BtnWnd := GetDlgItem ( hwndParent, BtnNum );
  GetWindowRect( BtnWnd, BtnRect );
  MapWindowPoints( HWND_DESKTOP, hwndParent, BtnRect, 2);
  
  ShowWindow( BtnWnd, SW_HIDE );
  
  
  if Left <> 0 then
    BtnOldRect.Left := Left
  else
    BtnOldRect.Left := BtnRect.Left;
	
  if Top <> 0 then
    BtnOldRect.Top := Top
  else
    BtnOldRect.Top := BtnRect.Top;
	
  if Width <> 0 then
    BtnOldRect.Width := Width
  else
    BtnOldRect.Width := BtnRect.Right - BtnRect.Left;
	
  if Height <> 0 then
    BtnOldRect.Height := Height
  else
    BtnOldRect.Height := BtnRect.Bottom - BtnRect.Top;
  
  BtnLen := 0;
  BtnGetText( BtnWnd, PChar( BtnText ), BtnLen );
  if BtnLen > 0 then
  begin
	SetLength(BtnText, BtnLen);
    ZeroMemory(@BtnText[1], BtnLen);
	BtnGetText( BtnWnd, PChar( BtnText ), BtnLen );
  end;
  
  //MessageBox( 0, '123', '321', MB_OK );

  return := BtnCreate( hwndParent, BtnOldRect.Left, BtnOldRect.Top, BtnOldRect.Width, BtnOldRect.Height, PChar( FileName ), 0, False );
  PushInt( return );
end;

procedure BtnRefresh_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
var
  h: HWND;
begin
  Init( hwndParent, string_size, variables, stacktop, extraparameters );
  h := PopInt;

  BtnRefresh( h );
end;

procedure BtnSetText_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
var
  h: HWND;
  Text: string;
begin
  Init( hwndParent, string_size, variables, stacktop, extraparameters );
  h := PopInt;
  Text := PopString;

  BtnSetText( h, PChar( Text ) );
end;

procedure BtnGetText_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
var
  h: HWND;
  Text: string;
  len: integer;
begin
  Init( hwndParent, string_size, variables, stacktop, extraparameters );
  h := PopInt;

  len := 0;
  BtnGetText( h, PChar( Text ), len );
  PushString( Text );
end;

procedure BtnSetTextAlignment_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
var
  h: HWND;
  HorIndent, VertIndent: integer;
  Alignment: DWORD;
begin
  Init( hwndParent, string_size, variables, stacktop, extraparameters );
  h := PopInt;
  HorIndent := PopInt;
  VertIndent := PopInt;
  Alignment := PopInt;

  BtnSetTextAlignment( h, HorIndent, VertIndent, Alignment );
end;

procedure BtnGetVisibility_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
var
  h: HWND;
  return: boolean;
begin
  Init( hwndParent, string_size, variables, stacktop, extraparameters );
  h := PopInt;

  return := BtnGetVisibility( h );
  PushInt( integer( return ) );
end;

procedure BtnSetVisibility_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
var
  h: HWND;
  Value: boolean;
begin
  Init( hwndParent, string_size, variables, stacktop, extraparameters );
  h := PopInt;
  Value := boolean( PopInt );

  BtnSetVisibility( h, Value );
end;

procedure BtnGetEnabled_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
var
  h: HWND;
  return: boolean;
begin
  Init( hwndParent, string_size, variables, stacktop, extraparameters );
  h := PopInt;

  return := BtnGetEnabled( h );
  PushInt( integer( return ) );
end;

procedure BtnSetEnabled_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
var
  h: HWND;
  Value: boolean;
begin
  Init( hwndParent, string_size, variables, stacktop, extraparameters );
  h := PopInt;
  Value := boolean( PopInt );

  BtnSetEnabled( h, Value );
end;

procedure BtnSetFont_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
var
  h: HWND;
  Font: HFONT;
begin
  Init( hwndParent, string_size, variables, stacktop, extraparameters );
  h := PopInt;
  Font := HFONT( PopInt );

  BtnSetFont( h, Font );
end;

procedure BtnSetFontColor_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
var
  h: HWND;
  NormalFontColor, FocusedFontColor, PressedFontColor, DisabledFontColor: integer;
begin
  Init( hwndParent, string_size, variables, stacktop, extraparameters );
  h := PopInt;
  NormalFontColor := PopInt;
  FocusedFontColor := PopInt;
  PressedFontColor := PopInt;
  DisabledFontColor := PopInt;

  BtnSetFontColor( h, NormalFontColor, FocusedFontColor, PressedFontColor, DisabledFontColor );
end;

procedure BtnGetChecked_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
var
  h: HWND;
  return: boolean;
begin
  Init( hwndParent, string_size, variables, stacktop, extraparameters );
  h := PopInt;

  return := BtnGetChecked( h );
  PushInt( integer( return ) );
end;

procedure BtnSetChecked_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
var
  h: HWND;
  Value: boolean;
begin
  Init( hwndParent, string_size, variables, stacktop, extraparameters );
  h := PopInt;
  Value := boolean( PopInt );

  BtnSetChecked( h, Value );
end;

procedure BtnSetEvent_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
var
  h: HWND;
  EventID: integer;
  Event: integer;
begin
  Init( hwndParent, string_size, variables, stacktop, extraparameters );
  h := PopInt;
  EventID := PopInt;
  Event := PopInt;

  BtnSetEvent( h, EventID, Event );
end;

procedure BtnSetPosition_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
var
  h: HWND;
  NewLeft, NewTop, NewWidth, NewHeight: integer;
begin
  Init( hwndParent, string_size, variables, stacktop, extraparameters );
  h := PopInt;
  NewLeft := PopInt;
  NewTop := PopInt;
  NewWidth := PopInt;
  NewHeight := PopInt;

  BtnSetPosition( h, NewLeft, NewTop, NewWidth, NewHeight );
end;

procedure BtnGetPosition_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
var
  h: HWND;
  Left, Top, Width, Height: integer;
begin
  Init( hwndParent, string_size, variables, stacktop, extraparameters );
  h := PopInt;

  BtnGetPosition( h, Left, Top, Width, Height );
  PushInt( Left );
  PushInt( Top );
  PushInt( Width );
  PushInt( Height );
end;

procedure BtnSetCursor_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
var
  h: HWND;
  hCur: HICON;
begin
  Init( hwndParent, string_size, variables, stacktop, extraparameters );
  h := PopInt;
  hCur := HICON( PopInt );

  BtnSetCursor( h, hCur );
end;

procedure GetSysCursorHandle_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
var
  id, return: integer;
begin
  Init( hwndParent, string_size, variables, stacktop, extraparameters );
  id := PopInt;

  return := GetSysCursorHandle( id );
  PushInt( return );
end;
//------------------------------------------------------------------------------

procedure ImgLoad_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
var
  h: HWND;
  Wnd: integer;
  Left,Top,Width,Height: integer;
  FileName: string;
  Stretch,IsBkg: boolean;
begin
  Init( hwndParent, string_size, variables, stacktop, extraparameters );
  h := PopInt;
  FileName := PopString;
  Left := PopInt;
  Top := PopInt;
  Width := PopInt;
  Height := PopInt;
  Stretch := boolean( PopInt );
  IsBkg := boolean( PopInt );

  Wnd := ImgLoad( h, PChar( FileName ), Left, Top, Width, Height, Stretch, IsBkg );
  PushInt( Wnd );
end;

procedure ImgSetPosition_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
var
  img: integer;
  NewLeft, NewTop, NewWidth, NewHeight: integer;
begin
  Init( hwndParent, string_size, variables, stacktop, extraparameters );
  img := PopInt;
  NewLeft := PopInt;
  NewTop := PopInt;
  NewWidth := PopInt;
  NewHeight := PopInt;

  ImgSetPosition( img, NewLeft, NewTop, NewWidth, NewHeight );
end;

procedure ImgGetPosition_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
var
  img: integer;
  Left, Top, Width, Height: integer;
begin
  Init( hwndParent, string_size, variables, stacktop, extraparameters );
  img := PopInt;

  ImgGetPosition( img, Left, Top, Width, Height );
  PushInt( Left );
  PushInt( Top );
  PushInt( Width );
  PushInt( Height );
end;

procedure ImgSetVisibility_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
var
  img: integer;
  Visible: boolean;
begin
  Init( hwndParent, string_size, variables, stacktop, extraparameters );
  img := PopInt;
  Visible := boolean( PopInt );

  ImgSetVisibility( img, Visible );
end;

procedure ImgGetVisibility_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
var
  img: integer;
  return: boolean;
begin
  Init( hwndParent, string_size, variables, stacktop, extraparameters );
  img := PopInt;

  return := ImgGetVisibility( img );
  PushInt( integer( return ) );
end;

procedure ImgSetVisiblePart_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
var
  img: integer;
  NewLeft, NewTop, NewWidth, NewHeight: integer;
begin
  Init( hwndParent, string_size, variables, stacktop, extraparameters );
  img := PopInt;
  NewLeft := PopInt;
  NewTop := PopInt;
  NewWidth := PopInt;
  NewHeight := PopInt;

  ImgSetVisiblePart( img, NewLeft, NewTop, NewWidth, NewHeight );
end;

procedure ImgGetVisiblePart_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
var
  img: integer;
  Left, Top, Width, Height: integer;
begin
  Init( hwndParent, string_size, variables, stacktop, extraparameters );
  img := PopInt;

  ImgGetVisiblePart( img, Left, Top, Width, Height );
  PushInt( Left );
  PushInt( Top );
  PushInt( Width );
  PushInt( Height );
end;

procedure ImgSetTransparent_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
var
  img: integer;
  Value: integer;
begin
  Init( hwndParent, string_size, variables, stacktop, extraparameters );
  img := PopInt;
  Value := PopInt;

  ImgSetTransparent( img, Value );
end;

procedure ImgGetTransparent_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
var
  img: integer;
  return: integer;
begin
  Init( hwndParent, string_size, variables, stacktop, extraparameters );
  img := PopInt;

  return := ImgGetTransparent( img );
  PushInt( return );
end;

procedure ImgRelease_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
var
  img: integer;
begin
  Init( hwndParent, string_size, variables, stacktop, extraparameters );
  img := PopInt;

  ImgRelease( img );
end;

procedure ImgApplyChanges_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
var
  h: HWND;
begin
  Init( hwndParent, string_size, variables, stacktop, extraparameters );
  h := PopInt;

  ImgApplyChanges( h );
end;
//------------------------------------------------------------------------------

procedure CreateFormFromImage_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
var
  h: HWND;
  FileName: string;
begin
  Init( hwndParent, string_size, variables, stacktop, extraparameters );
  h := PopInt;
  FileName := PopString;

  CreateFormFromImage( h, PChar( FileName ) );
end;

procedure CreateBitmapRgn_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
var
  DC: HDC;
  Bitmap: hBitmap;
  TransClr: TColorRef;
  dX: integer;
  dY: integer;
begin
  Init( hwndParent, string_size, variables, stacktop, extraparameters );
  DC := PopInt;
  Bitmap := PopInt;
  TransClr := PopInt;
  dX := PopInt;
  dY := PopInt;

  CreateBitmapRgn( DC, Bitmap, TransClr, dX, dY );
end;

procedure SetMinimizeAnimation_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
var
  Value: boolean;
begin
  Init( hwndParent, string_size, variables, stacktop, extraparameters );
  Value := boolean( PopInt );

  SetMinimizeAnimation( Value );
end;

procedure GetMinimizeAnimation_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
var
  return: boolean;
begin
  Init( hwndParent, string_size, variables, stacktop, extraparameters );

  return := GetMinimizeAnimation;
  PushInt( integer( return ) );
end;
//------------------------------------------------------------------------------

procedure gdipShutdown_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
begin
  gdipShutdown;
end;

procedure TestUnicode_( const hwndParent: HWND; const string_size: integer; const variables: PChar; const stacktop: pointer; const extraparameters: pointer = nil ) cdecl;
var
	TT1, TT2: string;
begin 
	TT1: = PopString;
	TT2: = PopString;
	MessageBox(g_hwndParent, PChar(TT1), PChar(TT2), 0);
end;

begin
end.